package com.example.demo;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.Resources;
import org.springframework.http.HttpMethod;
import org.springframework.integration.annotation.Gateway;
import org.springframework.integration.annotation.MessagingGateway;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.integration.dsl.IntegrationFlows;
import org.springframework.integration.dsl.Transformers;
import org.springframework.integration.http.dsl.Http;
import org.springframework.integration.scripting.dsl.Scripts;
import org.springframework.messaging.Message;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Data
class Plant {
    String _id;
    String name;
    String description;
    Integer price;
}

@MessagingGateway
interface RentalService {
    @Gateway(requestChannel = "req-channel", replyChannel = "rep-channel")
    Object findPlants(@Payload String name, @Header("startDate") LocalDate startDate, @Header("endDate") LocalDate endDate);
}

@Service
class CustomTransformer {
    @Autowired
    ObjectMapper mapper;

    public Resources<Resource<Plant>> fromJson(String json) {
        try {
            List<Plant> plants = mapper.readValue(json, new TypeReference<List<Plant>>() {});
            return new Resources<>(plants.stream().map(p -> new Resource<>(p, new Link("http://localhost:8088/api/v1/plant/" + p._id))).collect(Collectors.toList()));
        } catch (IOException e) {
            return null;
        }
    }

    public Resources<Resource<Plant>> fromHALForms(String json) {
        try {
            return mapper.readValue(json, new TypeReference<Resources<Resource<Plant>>>() {});
        } catch (IOException e) {
            return null;
        }
    }

}

//@Service
//class RentalServiceImpl {
//    RestTemplate restTemplate = new RestTemplate();
////    @Override
//    public String findPlants(Message<?> message) {
//        return restTemplate.getForObject("http://localhost:8088/api/v1/plant", String.class);
//    }
//}

@Configuration
class Flows {

    @Bean
    IntegrationFlow scatterComponent() {
        return IntegrationFlows.from("req-channel")
                .publishSubscribeChannel(conf ->
                        conf.applySequence(true)
                                .subscribe(f -> f.channel("rentmt-channel"))
                                .subscribe(f -> f.channel("rentit-channel"))
                )
                .get();
    }

    @Bean
    IntegrationFlow gatherComponent() {
        return IntegrationFlows.from("gather-channel")
                .aggregate(spec -> spec.outputProcessor(proc ->
                        new Resources<>(
                                proc.getMessages()
                                        .stream()
                                        .map(msg -> ((Resources) msg.getPayload()).getContent())
                                        .collect(Collectors.toList())))
                .groupTimeout(2000)
                        .releaseStrategy(group -> group.size() > 1)
                .sendPartialResultOnExpiry(true))
                .channel("rep-channel")
                .get();
    }

    @Bean
    IntegrationFlow rentMTFlow() {
        return IntegrationFlows.from("rentmt-channel")
                .handle(Http.outboundGateway("http://localhost:8088/api/v1/plant?filter[plant]=name==*{name}*")
                        .uriVariable("name", "payload")
                        .httpMethod(HttpMethod.GET)
                        .expectedResponseType(String.class)
                )
                .transform(Scripts.processor("classpath:/JsonApi2HAL.js")
                        .lang("javascript"))
                .handle("customTransformer", "fromJson")
                .channel("gather-channel")
                .get();
    }

    @Bean
    IntegrationFlow rentItFlow() {
        return IntegrationFlows.from("rentit-channel")
                .handle(Http.outboundGateway("http://localhost:8090/api/sales/plants?name={name}&startDate={startDate}&endDate={endDate}")
                        .uriVariable("name", "payload")
                        .uriVariable("startDate", "headers.startDate")
                        .uriVariable("endDate", "headers.endDate")
                        .httpMethod(HttpMethod.GET)
                        .expectedResponseType(String.class)
                )
                .handle("customTransformer", "fromHALForms")
                .channel("gather-channel")
                .get();
    }
}

@SpringBootApplication
public class DemoApplication {
    public static void main(String[] args) {
        ConfigurableApplicationContext ctx = SpringApplication.run(DemoApplication.class, args);
        RentalService service = ctx.getBean(RentalService.class);
        System.out.println(
                        service.findPlants("exc", LocalDate.now(), LocalDate.now().plusDays(2))
        );
    }
}